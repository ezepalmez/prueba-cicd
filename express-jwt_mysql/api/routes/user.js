const express = require('express');
const router = express.Router();

const mysqlConnection = require('../connection/connection');

const jwt = require('jsonwebtoken');


router.get('/', (req, res) => {
  mysqlConnection.query('select * from user', (err, rows, fields) => {
    if (!err) {
      res.json(rows);
    } else {
      console.log(err);
    }
  })
});

router.post('/singin', (req, res) => {
  const { username, password } = req.body;
  mysqlConnection.query('select username, role from user where username=? and password=?', [username, password],
    (err, rows, fields) => {
      if (!err) {
        if (rows.length > 0) {
          let data = JSON.stringify(rows[0]);
          const token = jwt.sign(data, 'wtk');
          res.json({ token, data });
        } else {
          res.json('Usuario o clave incorrectos');
        }
      } else {
        console.log('query '+ err);
      }
    }
  )
})

router.post('/test', verifyToken, (req, res) => {
  res.json('Informacion secreta');
})

function verifyToken(req, res, next) {
  if (!req.headers.authorization) return res.status(401).json('No autorizado');

  const token = req.headers.authorization.substr(7);

  if (token !== '') {
    const content = jwt.verify(token, 'wtk');
    req.data = content;
    next();
  } else {
    res.status(401).json('Token vacio');
  }

}

module.exports = router;