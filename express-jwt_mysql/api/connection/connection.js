const mysql = require('mysql');

const mysqlConnection = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: 'mysqlpassword',
  database: 'mrd_schema',
  //port: '8889'
});

mysqlConnection.connect(err => {
  if (err) {
    console.log('Error en db: ', err);
    return;
  } else {
    console.log('Db ok');
  }
});

module.exports = mysqlConnection;