import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { User } from 'src/app/model/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  /*user = {
    userName: 'kevin',
    pass: '123'
  }*/
  usuario: User = new User;
  user= '';
  pass= '';

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  logIn() {
    this.usuario.username = this.user;
    this.usuario.password = this.pass;

    this.authService.singin(this.usuario).subscribe((res: any) => {
      console.log(res.data);
      localStorage.setItem('token', res.token);

     /* var obj = JSON.parse(res.data);
      var role = obj['role'];
      console.log('--->> '+role)*/

      this.router.navigate(['home']);
    })
  }

}
